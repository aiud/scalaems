name := "EMS"

version := "0.1"

scalaVersion := "2.13.2"

publish/skip := true

enablePlugins(FlywayPlugin)

libraryDependencies ++= Dependencies.emsDependencies

flywayUrl :=  "jdbc:postgresql://localhost:5432/ems"
flywayUser := "postgres"
flywayPassword := "postgres"
flywayLocations += "db/migration"

//flywayUrl in Test := "jdbc:hsqldb:file:target/flyway_sample;shutdown=true"
//flywayUser in Test := "SA"
//flywayPassword in Test := ""

scalacOptions ++= Seq(
  "-Xfatal-warnings",
  "-language:higherKinds"
)
