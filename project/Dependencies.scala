import sbt.Keys.resolvers
import sbt._

object Dependencies {

  val CatsVersion = "2.1.1"
  val CirceVersion = "0.13.0"
  val CirceGenericExVersion = "0.13.0"
  val CirceConfigVersion = "0.8.0"
  val DoobieVersion = "0.9.0"
  val EnumeratumCirceVersion = "1.6.0"
  val H2Version = "1.4.200"
  val Http4sVersion = "0.21.4"
  val KindProjectorVersion = "0.11.0"
  val LogbackVersion = "1.2.3"
  val ScalaCheckVersion = "1.14.3"
  val ScalaTestVersion = "3.1.1"
  val ScalaTestPlusVersion = "3.1.1.1"
  val FlywayVersion = "6.4.1"
  val TsecVersion = "0.2.0"
  val JodaTime = "2.10.6"
  val CheckDateTime = "0.3.5"
  val Postgress = "42.1.1"
  val ScalaCheckShapeless = "1.2.5"

  /**Cats*/
  val catsCore = "org.typelevel" %% "cats-core" % CatsVersion

  /** HTTP4S dependencies */
  val http4sdsl = "org.http4s" %% "http4s-dsl" % Http4sVersion
  val http4blazeserver = "org.http4s" %% "http4s-blaze-server" % Http4sVersion
  val http4blazeclient =  "org.http4s" %% "http4s-blaze-client" % Http4sVersion
  val http4sCirce = "org.http4s" %% "http4s-circe" % Http4sVersion

  /** Doobie dependencies */
  val doobieCore = "org.tpolecat" %% "doobie-core" % DoobieVersion
  val shapeless = "com.chuusai" %% "shapeless" % "2.3.3"
  val doobiePostgres = "org.tpolecat" %% "doobie-postgres" % DoobieVersion // Postgres driver 42.2.9 + type mappings.
  val quill = "org.tpolecat" %% "doobie-quill" % DoobieVersion //
  val doobieScalaTest = "org.tpolecat" %% "doobie-scalatest" % DoobieVersion
  val hikari = "org.tpolecat" %% "doobie-hikari" % DoobieVersion

  /** Circe dependencies */
  val circeGeneric = "io.circe" %% "circe-generic" % CirceVersion
  val circeLiteral = "io.circe" %% "circe-literal" % CirceVersion
  val circeGenExtras = "io.circe" %% "circe-generic-extras" % CirceGenericExVersion
  val circeParser = "io.circe" %% "circe-parser" % CirceVersion
  val circeConf = "io.circe" %% "circe-config" % CirceConfigVersion
  val enumeratumCirce = "com.beachape" %% "enumeratum-circe" % EnumeratumCirceVersion

  /** Test dependencies*/
  val scalaTest = "org.scalatest" %% "scalatest" % ScalaTestVersion % Test
  val jodaTime = "joda-time" % "joda-time" % JodaTime % Test
  val scalacheck = "org.scalacheck" %% "scalacheck" % ScalaCheckVersion % Test
  val scalacheckDatetime = "com.47deg" %% "scalacheck-toolbox-datetime" % CheckDateTime % Test
  val scalacheckShapeless = "com.github.alexarchambault" %% "scalacheck-shapeless_1.14" % "1.2.5" % Test
  val scalatestplusScheck = "org.scalatestplus" %% "scalacheck-1-14" % ScalaTestPlusVersion % Test

  /** Database  */
  val postgresDb = "org.postgresql" % "postgresql" %  Postgress

  lazy val emsDependencies: Seq[ModuleID] = Seq(
    catsCore,
    http4sdsl,
    http4blazeserver,
    http4sCirce,
    doobieCore,
    shapeless,
    doobiePostgres,
    quill,
    doobieScalaTest,
    hikari,
    circeGeneric,
    circeLiteral,
    circeGenExtras,
    circeParser,
    circeConf,
    enumeratumCirce,
    scalaTest,
    jodaTime,
    scalacheck,
    scalacheckDatetime,
    scalacheckShapeless,
    scalatestplusScheck,
    postgresDb )
}

