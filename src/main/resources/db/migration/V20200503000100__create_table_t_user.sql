create TABLE t_user
(
    user_id             BIGSERIAL PRIMARY KEY,
    user_name           VARCHAR(25)        NOT NULL,
    email               VARCHAR(25) UNIQUE NOT NULL,
    telephone_number    VARCHAR(20) UNIQUE,
    address             VARCHAR(20)        NOT NULL,
    encrypted_password  VARCHAR(255) NOT NULL
);
COMMENT ON TABLE t_user IS 'user.  References with events';
COMMENT ON COLUMN t_user.user_id IS 'Primary key of t_users table.';
COMMENT ON COLUMN t_user.user_name IS 'name of the t_users. A not null column.';
COMMENT ON COLUMN t_user.email IS 'Email id of the t_users';
COMMENT ON COLUMN t_user.telephone_number IS 'Phone number of the t_users; includes country code and area code';
COMMENT ON COLUMN t_user.encrypted_password IS 'password of the user';