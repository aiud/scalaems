create TABLE t_event
(
    event_id         BIGSERIAL PRIMARY KEY,
    date_time        TIMESTAMP    NOT NULL,
    description      VARCHAR(255),
    event_name       VARCHAR(255)  NOT NULL,
    total_places     INT,
    available_places INT,
    location         VARCHAR(10)  NOT NULL
);
COMMENT ON TABLE  t_event IS 't_events table.';
COMMENT ON COLUMN t_event.event_id IS 'Primary key of t_events table.';
COMMENT ON COLUMN t_event.event_name IS 'name of the event. A not null column.';
COMMENT ON COLUMN t_event.description IS 'description of event';
COMMENT ON COLUMN t_event.total_places IS 'total places available for a event';
COMMENT ON COLUMN t_event.location IS 'location where event will take place';
COMMENT ON COLUMN t_event.date_time IS 'Date and time when event will take place';