create TABLE t_user_event
(
    user_id      BIGINT  references t_user(user_id),
    event_id     BIGINT  references t_event(event_id)
);