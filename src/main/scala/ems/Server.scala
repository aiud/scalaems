package ems
import cats.effect._
import org.http4s.HttpRoutes
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.blaze._

import scala.concurrent.ExecutionContext.global

object Server extends IOApp{

  val service = HttpRoutes.of[IO]{
    case GET -> Root/"hello"/name => Ok(s"Hello $name")
  }.orNotFound

  def run(args: List[String]): IO[ExitCode] =
    BlazeServerBuilder[IO](global)
      .bindHttp(8080, "localhost")
      .withHttpApp(service)
      .resource
      .use(_ => IO.never)
      .as(ExitCode.Success)
}
