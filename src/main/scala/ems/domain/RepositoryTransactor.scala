package ems.domain

import cats.effect.{Blocker, IO, Resource}
import doobie.ExecutionContexts
import doobie.hikari.HikariTransactor

import scala.concurrent.ExecutionContext.global

object RepositoryTransactor {

  implicit  val contextShift = IO.contextShift(global)

  val transactor : Resource[IO, HikariTransactor[IO]] =
    for{
      ec <- ExecutionContexts.fixedThreadPool[IO](32)
      blocker <- Blocker[IO]
      xa <-   HikariTransactor.newHikariTransactor[IO](
        "org.postgresql.Driver",
        "jdbc:postgresql://localhost:5432/ems",
        "postgres",
        "postgres",
        ec,
        blocker
      )
    } yield xa


}
