package ems.domain.users

case class User(
                 userId: Option[Long],
                 userName: String,
                 email: String,
                 telephoneNumber: String,
                 address: String,
                 encryptedPassword: String
)
