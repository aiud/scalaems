package ems.domain.users

import cats.data.OptionT

trait UserRepositoryAlgebra[F[_]] {

def create(user : User) : F[User]
def getUserById(id : Long) : OptionT[F, User]
def getAllUsers : F[List[User]]
def findByUserName(name : String) : OptionT[F, User]

}
