package ems.insrastructure.repository.doobie

import cats.data.OptionT
import cats.effect.{Bracket, IO, Resource}
import doobie.hikari.HikariTransactor
import doobie.util.query.Query0
import doobie.util.transactor.Transactor
import doobie.util.update.Update0
import ems.domain.users.{User, UserRepositoryAlgebra}
import cats.data._
import cats.implicits._
import doobie._
import doobie.implicits._
import cats.effect.Bracket

private object UserSql{

  def insert(user: User): ConnectionIO[User] = {
    sql"""INSERT  INTO t_user(user_name, email, telephone_number, address, encrypted_password)
         | VALUES (${user.userName}, ${user.email}, ${user.telephoneNumber}, ${user.address}, ${user.encryptedPassword})"""
         .stripMargin.update.withUniqueGeneratedKeys[User]("user_id", "user_name", "email", "telephone_number", "address", "encrypted_password")
  }

  def selectUserById(id : Long) : Query0[User] ={
    sql"""SELECT user_id, user_name, email, telephone_number, address, encrypted_password from t_user
         |where user_id = ${id}"""
         .stripMargin.query[User]
  }

  def selectUserByName(name : String) : Query0[User] ={
    sql"""SELECT user_id, user_name, email, telephone_number, address, encrypted_password from t_user
         |where user_name = ${name}"""
          .stripMargin.query
  }


}

class DoobieUserRepositoryInterpreter[F[_]](val transactor: Transactor[F]) extends UserRepositoryAlgebra[F] {

  import UserSql._

  def create(user: User): F[User] = ???


  def findByUserName(name: String): OptionT[F, User] = ???

  def getUserById(id: Long): OptionT[F, User] = ???

  def getAllUsers: F[List[User]] = ???


}
